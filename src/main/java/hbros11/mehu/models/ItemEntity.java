package hbros11.mehu.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity(name = "items")
public class ItemEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    @ManyToMany(mappedBy = "items")
    @JsonIgnoreProperties("items")
    private final Set<TableEntity> tables = new LinkedHashSet<>();

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<TableEntity> getTables() {
        return tables;
    }
}
