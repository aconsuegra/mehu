package hbros11.mehu.models;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity(name = "products")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    private String measurementUnit;

    @OneToMany(mappedBy = "productEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductPriceEntity> productPriceEntities = new LinkedHashSet<>();

    public Set<ProductPriceEntity> getProductPriceEntities() {
        return productPriceEntities;
    }

    public void setProductPriceEntities(Set<ProductPriceEntity> productPriceEntities) {
        this.productPriceEntities = productPriceEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ProductEntity that = (ProductEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
