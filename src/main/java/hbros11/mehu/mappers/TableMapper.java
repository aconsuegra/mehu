package hbros11.mehu.mappers;


import hbros11.mehu.models.TableEntity;
import hbross11.menhu.models.TableDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring", uses = ItemMapper.class)
public interface TableMapper {

    TableMapper INSTANCE = Mappers.getMapper(TableMapper.class);



    @Mapping(target ="items", qualifiedByName = "itemWithoutTables")
    TableDTO toTableDTO(TableEntity tableEntity);

    List<TableDTO> toListOfTableDTO(List<TableEntity> tableEntities);










}
