package hbros11.mehu.mappers;



import hbros11.mehu.models.ProductEntity;
import hbros11.mehu.models.ProductPriceEntity;
import hbross11.menhu.models.ProductDTO;
import hbross11.menhu.models.ProductPriceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductDTO toProductDTO(ProductEntity productEntity);

    List<ProductDTO> toListOfProductDTOs(List<ProductEntity> productEntities);

    ProductEntity toProductEntity(ProductDTO productDTO);

    @Mapping(source = "productEntity.id", target = "productId")
    ProductPriceDTO toProductPriceDTO(ProductPriceEntity productPriceEntity);

    List<ProductPriceDTO> toListOfProductPriceDTO(List<ProductPriceEntity> productPriceEntities);

}
