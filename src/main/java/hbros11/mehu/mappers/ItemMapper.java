package hbros11.mehu.mappers;


import hbros11.mehu.models.ItemEntity;
import hbross11.menhu.models.ItemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ItemMapper {

    ItemMapper INSTANCE = Mappers.getMapper(ItemMapper.class);


    @Named("itemWithoutTables")
    @Mapping(target = "tables", ignore = true)
    ItemDTO toItemNoTables(ItemEntity itemEntity);


    ItemDTO toItem(ItemEntity itemEntity);

}
