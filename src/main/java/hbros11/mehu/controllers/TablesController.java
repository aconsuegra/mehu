package hbros11.mehu.controllers;


import hbros11.mehu.mappers.TableMapper;
import hbros11.mehu.repositories.TableRepository;
import hbross11.menhu.api.TablesApi;
import hbross11.menhu.models.TableDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TablesController  implements TablesApi {

    @Autowired
    TableRepository tableRepository;
    @Autowired
    private TableMapper tableMapper;

    @Override
    public List<TableDTO> tables() {
        return tableMapper.toListOfTableDTO(tableRepository.findAll());
    }
}
