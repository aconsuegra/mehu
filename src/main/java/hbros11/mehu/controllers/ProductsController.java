package hbros11.mehu.controllers;

import hbros11.mehu.mappers.ProductMapper;
import hbros11.mehu.repositories.ProductPriceRepository;
import hbros11.mehu.repositories.ProductRepository;
import hbross11.menhu.api.ProductsApi;
import hbross11.menhu.models.ProductDTO;
import hbross11.menhu.models.ProductPriceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class ProductsController implements ProductsApi {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductPriceRepository productPriceRepository;

    @Autowired
    ProductMapper productMapper;


    @Override
    public ProductDTO create(@RequestBody ProductDTO productDTO) {

        var product = productRepository.save(productMapper.toProductEntity(productDTO));
        return productMapper.toProductDTO(product);

    }


    @Override
    public List<ProductDTO> index() {
        return productMapper.toListOfProductDTOs(productRepository.findAll());
    }



    @Override
    public ProductPriceDTO addProductPrice(@RequestBody ProductPriceDTO productPriceDTO) {

        var id = productPriceRepository.nativeSave(productPriceDTO.getCreatedAt(), productPriceDTO.getPrice(),
                productPriceDTO.getLatitude(), productPriceDTO.getLongitude(), productPriceDTO.getProductId());
        productPriceDTO.setId(id);

        return productPriceDTO;
    }



    @Override
    public List<ProductPriceDTO> getProductPrices(Long productId) {
        return productMapper.toListOfProductPriceDTO(productPriceRepository.findByProductId(productId));

    }


}
