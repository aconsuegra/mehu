package hbros11.mehu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MehuApplication {

	public static void main(String[] args) {
		SpringApplication.run(MehuApplication.class, args);
	}

}
