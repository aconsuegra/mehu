package hbros11.mehu.repositories;

import hbros11.mehu.models.ProductPriceEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface ProductPriceRepository extends JpaRepository<ProductPriceEntity, Long> {

    @Transactional
    @Query(value = "INSERT INTO product_prices(created_at, price, latitude, longitude, product_id) " +
            "VALUES (:created_at, :price, :latitude, :longitude, :product_id) RETURNING id", nativeQuery = true)
    Long nativeSave(@Param(value = "created_at") LocalDate createdAt, @Param(value = "price") BigDecimal price,
                    @Param(value = "latitude") double latitude, @Param(value = "longitude") double longitude,
                    @Param(value = "product_id") Long productId);

    @Query(value = "SELECT * FROM product_prices pp WHERE pp.product_id = :product_id " +
            "ORDER BY created_at ASC", nativeQuery = true)
    List<ProductPriceEntity> findByProductId(@Param("product_id") long productId);

}
