package hbros11.mehu.repositories;

import hbros11.mehu.models.TableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public  interface TableRepository  extends JpaRepository<TableEntity, Long> {
}
