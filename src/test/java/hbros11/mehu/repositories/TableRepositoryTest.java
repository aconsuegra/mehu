package hbros11.mehu.repositories;

import hbros11.mehu.models.TableEntity;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@Tag("db-tests")
class TableRepositoryTest {

    @Autowired
    DataSource dataSource;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    EntityManager entityManager;
    @Autowired
    TableRepository tableRepository;


    @BeforeEach
    void setUp() {

    }

    @Test
    void injectedComponentsAreNotNull() {
        assertNotNull(dataSource);
        assertNotNull(jdbcTemplate);
        assertNotNull(entityManager);
        assertNotNull(tableRepository);
    }

    @Test
    void testSaveEntity() {
        var table = new TableEntity();
        table.setName("Sample table 1");
        table.setAvailable(true);
        var savedTable = tableRepository.save(table);

        assertEquals(table, savedTable);
        assertEquals(1, tableRepository.findAll().size());
    }

    @Test
    void testFindAll() {
        assertEquals(0, tableRepository.findAll().size());
    }
}