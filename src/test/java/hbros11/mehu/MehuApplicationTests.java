package hbros11.mehu;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Tag("integration-tests")
class MehuApplicationTests {

	@Test
	void contextLoads() {
	}

}
