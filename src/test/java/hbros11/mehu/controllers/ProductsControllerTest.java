package hbros11.mehu.controllers;

import hbros11.mehu.mappers.ProductMapper;
import hbros11.mehu.repositories.ProductPriceRepository;
import hbros11.mehu.repositories.ProductRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@Tag("unit-tests")
class ProductsControllerTest {
    @Mock
    ProductRepository productRepository;

    @Mock
    ProductPriceRepository productPriceRepository;

    @Mock
    ProductMapper productMapper;

    @InjectMocks
    ProductsController productsController;

    @Test
    void get_product_prices() {

        productsController.getProductPrices(1L);

        verify(productPriceRepository).findByProductId(1);
        verify(productMapper).toListOfProductPriceDTO(anyList());

    }
}